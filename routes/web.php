<?php

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->post('/login', 'Auth\LoginController@login');
$router->post('/register', 'Actor\UserController@register');
$router->get('/user', ['middleware' => 'auth', 'uses' =>  'Actor\UserController@get_user']);
